
var glob_points = [];
var glob_rectangles = [];
var glob_overlaps = [];
var glob_method;
var glob_loop_coefficient = 10;
var glob_point_offset = 10;

function getWindowSize()
{
    var size = {};
    var canvas = $("#layMap");
    var width = canvas.width();
    var height = canvas.height();
    size.width = width;
    size.height = height;
    return size;
}


function printCanvasSize()
{
    var txtSize = $("#canvasSize");
    var size = getWindowSize();
    var strSize = "w: " + size.width + ", h: " + size.height;
    txtSize.text(strSize);
}


function randomRange(min, max)
{
    return round(Math.random() * (max - min + 1) + min);
}


function randomRect()
{
    let rect = {};
    let width = 90;
    let height = 20;

    rect.x = 0;
    rect.y = 0;
    rect.width = width;
    rect.height = height;
    return rect;
}


function randomPoint()
{
    var size = getWindowSize();
    var point = {};
    point.x = randomRange(0, size.width);
    point.y = randomRange(0, size.height);
    return point;
}


function appendPoint()
{
    var point = randomPoint();
    var rect = randomRect();
    glob_points.push(point);
    glob_rectangles.push(rect);
}


var appendPoints = function (addCount)
{
    for(var i=0; i < addCount; i++)
    {
        appendPoint();
    }
};


function redraw()
{
    // Compute rectangle positions and draw
    drawRectangles();

    // Compute rect and point overlaps
    computeOverlapArray(glob_rectangles, glob_overlaps);

    // Draw the rest
    drawPoints();
    drawLines();
    drawOverlaps();
}

function placePoints()
{
    for(var i=0; i<glob_points.length; i++)
    {
        var point = $('<div class="point">'+i+'</div>');

        point.css({
            position: 'absolute',
            left: glob_points[i].x,
            top: glob_points[i].y
        });

        point.draggable({
            start: function(event, ui)
            {
                var id = Number($(this).text());
                $("[data-id='"+id+"']").hide();
            },
            drag: function(event, ui)
            {

                var id = Number($(this).text());
                glob_points[id].x = round(ui.position.left);
                glob_points[id].y = round(ui.position.top);
                redraw();

                $("[data-id='"+id+"']").show();


            },
            stop: function (event, ui)
            {
                var id = Number($(this).text());
                glob_points[id].x = round(ui.position.left);
                glob_points[id].y = round(ui.position.top);
                redraw();
                $("[data-id='"+id+"']").show();
            }
        });

        if(glob_points[i].overlapped === true)
        {
            point.addClass("overlapped");
        }

        $("#mapCanvas").append(point);
    }
}


function createGuiRect(rect, num)
{
    var guiRect = $('<div class="rectangle">'+num+'</div>');

    guiRect.css({
        position: 'absolute',
        left: rect.x,
        top: rect.y,
        width: rect.width,
        height: rect.height
    });

    return guiRect;
}


function drawOverlaps()
{
    var canvas = $("#mapCanvas");
    canvas.find("div.overlap").remove();

    // For each pair (one time)
    for(var i=0; i<glob_overlaps.length; i++)
    {
        var rect = createGuiRect(glob_overlaps[i], "");
        canvas.append(rect);
        rect.addClass("overlap");
    }
}

function placeRectangles()
{
    var size = getWindowSize();

    window[glob_method](glob_points, glob_rectangles, size.width, size.height);

    var canvas = $("#mapCanvas");
    canvas.find(".rectangle").remove();
    for(var i=0; i<glob_rectangles.length; i++)
    {
        var rect = createGuiRect(glob_rectangles[i], i);
        rect.attr("data-id", String(i));
        canvas.append(rect);
    }
}


function drawPoints()
{
    $("#mapCanvas").find(".point").remove();
    placePoints();
}


function drawRectangles()
{
    placeRectangles();
    sumOverlap();
}


function placeLines()
{
    for(var i=0; i<glob_points.length; i++)
    {
        var point = glob_points[i];
        var rect = glob_rectangles[i];

        var rectCenter = {};
        rectCenter.x = rect.x + rect.width/2.0;
        rectCenter.y = rect.y + rect.height/2.0;

        var newLine = document.createElementNS('http://www.w3.org/2000/svg','line');
        newLine.setAttribute('id','line');
        newLine.setAttribute('x1',''+point.x+'');
        newLine.setAttribute('y1',''+point.y+'');
        newLine.setAttribute('x2',''+rectCenter.x+'');
        newLine.setAttribute('y2',''+rectCenter.y+'');
        newLine.setAttribute('data-id', String(i));

        if(point.lineOverlapped === true)
        {
            newLine.setAttribute('style', 'stroke:rgb(255,0,0)');
        }

        $("#svgCanvas").append(newLine);
    }
}

function computeOverlapArray(inputRectArray, outputRectArray)
{
    // Empty overlaps
    while(outputRectArray.length > 0) {
        outputRectArray.pop();
    }

    for(var i=0; i<inputRectArray.length; i++)
    {
        // Each pair only once
        for (var j = i + 1; j < inputRectArray.length; j++)
        {
            var overlap = computeOverlapRect(inputRectArray[i], inputRectArray[j]);
            var area = overlap.width * overlap.height;

            if(area > 0)
            {
                outputRectArray.push(overlap);
            }
        }
    }
}

function drawLines()
{
    $("#svgCanvas").find("line").remove();
    placeLines();
}

function sumOverlap()
{
    var area = 0;
    for(var i=0; i<glob_rectangles.length; i++)
    {
        area += glob_rectangles[i].overlapArea;
    }
    $("#overlapArea").html(String(Math.floor(area)));
}


function onBtnAdd()
{
    var text = $(this).text();
    var addCount = Number(text.substring(1));

    appendPoints(addCount);
    redraw();
}


function clearAll()
{
    glob_points = [];
    glob_rectangles = [];
    redraw();
}

function shufflePoints()
{
    for(var i=0; i<glob_points.length; i++)
    {
        var position = randomPoint();
        glob_points[i].x = position.x;
        glob_points[i].y = position.y;
    }
    redraw();
}

function setupButtons()
{
    $(".btnAdd").click(onBtnAdd);
    $("#btnClear").click(clearAll);
    $("#btnRedraw").click(redraw);
    $("#btnShuffle").click(shufflePoints);
}

function onMethodSelected()
{
    glob_method = $("#selMethod").find("option:selected").text();
    redraw();
}

function setupMethods()
{
    var methods = [
        "topRight",
        "bruteForce",
        "random",
        "discreteGradientDescent",
        "singleGradientDescent",
        "SGD_one_step",
        "multiGradientDescent",
        "DGD_MGD",
        "SGD_MGD",
        "simulatedAnnealing",
        "forceVectors"
    ];

    glob_method = methods[0];

    $.each(methods, function (i, item)
    {
        $('#selMethod').append($('<option>', {
            value: item,
            text : item
        }));
    });

    $("#selMethod").change(onMethodSelected);

    // Set default algorithm.
    $('#selMethod').val(methods[methods.length-1]);
    onMethodSelected();
}

$(document).ready(function()
{
    printCanvasSize();
    $(window).resize(printCanvasSize);

    setupButtons();
    setupMethods();
});