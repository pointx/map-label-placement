
/**
 * Brute force labeling
 * Try all combinations and return the one with the lowest fitness function
 */
function bruteForce(points, rectangles, width, height)
{
    // 'optimal' situation - each point has the top priority position.
    var solution = zeroArray(rectangles.length);

    findSolution(solution, points, rectangles, width, height);
    setPositions(points, rectangles, solution);
    computeOverlaps(rectangles, points, width, height);
}

var glob_fitness;
var glob_solution;
function findSolution(solution, points, rectangles, width, height)
{
    glob_fitness = Number.POSITIVE_INFINITY;

    var candidate = solution.slice();
    findSolutionRec(candidate, points, rectangles, width, height, 0);
    
    for(var i=0; i<solution.length; i++)
    {
        solution[i] = glob_solution[i];
    }
}

function findSolutionRec(candidate, points, rectangles, width, height, index)
{
    // Recursion end
    if(index >= rectangles.length)
    {
        var fitness = discreteFitness(candidate, points, rectangles, width, height);
        
        // Better solution found
        if(fitness < glob_fitness)
        {
            console.log("Candidate: " + candidate);
            console.log("Fitness: " + fitness);
            glob_solution = candidate.slice();
            glob_fitness = fitness;
        }

        return;
    }

    // Candidates generation
    for(var pos=0; pos<8; pos++)
    {
        candidate[index] = pos;
        findSolutionRec(candidate, points, rectangles, width, height, index+1);
    }
}