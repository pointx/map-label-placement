
/**
 * Vector gradient descent labeling
 * Every step every overlapping rectangle is moved
 */
function multiGradientDescent(points, rectangles, width, height)
{
    // Start from all-zero solution
    var solution = zeroArray(rectangles.length);
    setPositions(points, rectangles, solution);

    // Compute gradient - best position change that improves total overlap
    // Note: since we use continuous space, we can not use predefined positions
    // anymore.
    while(true)
    {
        var fitnessDelta = multiGradientStep(points, rectangles, width, height);
        if(fitnessDelta >= 0) break;
    }

    // Finish computing
    computeOverlaps(rectangles, points, width, height);
}


function addRectOverlapDelta(rectA, rectB)
{
    var rect = computeOverlapRect(rectA, rectB);

    // No overlap, skip.
    if(rect.width === 0 || rect.height === 0) return;

    // Overlap found. Move both rectangles  half the distance to
    // eliminate this overlap, but prefer more the shorter one.
    var deltaX = 0;
    var deltaY = 0;
    if(rect.width > rect.height) {
        deltaX = Math.sign(rect.width);
        deltaY = rect.height;
    } else {
        deltaX = rect.width;
        deltaY = Math.sign(rect.height);
    }

    // Each rectangle will move a half of the delta, so we have to
    // determine direction of the move based od rectangle centers.
    var centerI = rectangleCenter(rectA);
    var centerJ = rectangleCenter(rectB);
    var centerOverlap = rectangleCenter(rect);

    // x-direction and y-direction.
    var dirXI = Math.sign( centerI.x - centerOverlap.x );
    var dirYI = Math.sign( centerI.y - centerOverlap.y );
    var dirXJ = Math.sign( centerJ.x - centerOverlap.x );
    var dirYJ = Math.sign( centerJ.y - centerOverlap.y );

    // If both rectangles have the same x or y position, direction is 0,
    // so we have to decide which one rectangle will move which
    // direction - the lower index has priority to move up / left.
    if(dirXI === dirXJ === 0)
    {
        dirXI = -1;
        dirXJ = 1;
    }
    if(dirYI === 0 && dirYJ === 0)
    {
        dirYI = -1;
        dirYJ = 1;
    }

    // Add to movement vectors.
    rectA.deltaX += dirXI * deltaX;
    rectA.deltaY += dirYI * deltaY;
    rectB.deltaX += dirXJ * deltaX;
    rectB.deltaY += dirYJ * deltaY;
}

function addPointOverlapDelta(point, rect)
{
    // No overlap, skip.
    if(!isPointInRect(point, rect)) return;

    // Move rectangle shortest path possible. Compute distances to all four
    // directions and pick the shortest.

    var deltaX = 0;
    var deltaY = 0;

    var top = point.y - rect.y + 1;                  // positive
    var bottom = point.y - rect.y - rect.height - 1; // negative
    var left = point.x - rect.x + 1;                 // positive
    var right = point.x - rect.x - rect.width - 1;   // negative

    // If equal, keep 0
    if(Math.abs(top) < Math.abs(bottom)) {
        deltaY = top;
    } else if(Math.abs(top) > Math.abs(bottom)){
        deltaY = bottom;
    }

    if(Math.abs(left) < Math.abs(right)){
        deltaX = left;
    } else if(Math.abs(left) > Math.abs(right)) {
        deltaX = right;
    }

    // Prefer shorter move
    if(Math.abs(deltaX) < Math.abs(deltaY)) {
        deltaY /= 10;
    } else {
        deltaX /= 10;
    }

    rect.deltaX += deltaX + Math.sign(deltaX) * glob_point_offset/2;
    rect.deltaY += deltaY + Math.sign(deltaY) * glob_point_offset/2;
}

function addScreenOverlapDelta(rect, width, height)
{
    // Top
    rect.deltaY += Math.max(-rect.y, 0);

    // Right
    rect.deltaX -= Math.max(rect.x + rect.width - width, 0);

    // Bottom
    rect.deltaY -= Math.max(rect.y + rect.height - height, 0);

    // Left
    rect.deltaX += Math.max(-rect.x, 0);
}

function multiGradientStep(points, rectangles, width, height)
{
    var actualFitness = continuousFitness(points, rectangles, width, height);

    for(var i=0; i<rectangles.length; i++)
    {
        // Start with zero movement
        rectangles[i].deltaX = 0;
        rectangles[i].deltaY = 0;
    }

    // Compare each pair of rectangles and compute movement vector for
    // each rectangle
    for(i=0; i<rectangles.length; i++)
    {
        for(var j=i+1; j<rectangles.length; j++)
        {
            addRectOverlapDelta(rectangles[i], rectangles[j]);
            addPointOverlapDelta(points[i], rectangles[j]);
            addPointOverlapDelta(points[j], rectangles[i]);
        }
        addScreenOverlapDelta(rectangles[i], width, height)
    }

    // Apply deltas, compute improvement and if the fitness is not better, undo.
    for(i=0; i<rectangles.length; i++)
    {
        rectangles[i].x += rectangles[i].deltaX;
        rectangles[i].y += rectangles[i].deltaY;
    }
    var fitness = continuousFitness(points, rectangles, width, height);
    var improvement = fitness - actualFitness;

    // The same or worse solution
    if(improvement >= 0)
    {
        // Undo deltas
        for(i=0; i<rectangles.length; i++)
        {
            rectangles[i].x -= rectangles[i].deltaX;
            rectangles[i].y -= rectangles[i].deltaY;
        }
    }

    return improvement;
}