

function fanComputer(points, rectangles, width, height)
{
    forceVectors(points, rectangles, width, height);
}


// Parameters that adjust behaviour of the force vector algorithm.
const defaultParams = {

    // Margins (in pixels) around rectangles to avoid touching each other,
    // it looks better.
    rectMargin: 2.5,

    // Minimum distance (in pixels) of two points that can affect each
    // other.
    minForceDistance: 200,

    // Initial length of the line that connects the point and the rectangle.
    lineLength: 60,

    // Radius (in pixels) around points to avoid labels to be too near
    // of points.
    pointRadius: 20
};


/**
 * Labeling using 'force vectors'.
 *
 * Input:
 *
 * - points (n-sized list of objects with x and y numbers)
 * - rectangles (n-sized list of objects with x, y, width, height)
 * - canvas dimensions: width, height
 * - force vector parameters
 *
 * Output:
 *
 * - rectangles have set x and y position that was computed
 * - each rectangle has new attribute: overlapArea
 *
 */
function forceVectors(points, rectangles, width, height, params = defaultParams)
{
    addMargins(rectangles, params.rectMargin);
    initDirectionVectors(points);
    computeDirectionVectors(points, params);
    normalizeDirectionVectors(points);
    directionVectorsToPosition(points, rectangles, params);
    computeOverlaps(rectangles, points, width, height);

    // Fix little overlaps
    singleGradientDescent(points, rectangles, width, height, params);

    // Restore margins. In case that the rectangles are further used, restore
    // their initial sizes.
    addMargins(rectangles, -params.rectMargin);
}


/**
 * Manually increase size of rectangles to prevent them to touch each other
 * (does not look nice).
 */
function addMargins(rectangles, margin)
{
    for(let i=0; i<rectangles.length; i++)
    {
        rectangles[i].x -= margin;
        rectangles[i].y -= margin;
        rectangles[i].width += 2*margin;
        rectangles[i].height += 2*margin;
    }
}


/**
 * Reset direction vectors to zero.
 */
function initDirectionVectors(points)
{
    for(let i=0; i<points.length; i++)
    {
        points[i].vectorX = 0;
        points[i].vectorY = 0;
    }
}


/**
 * For each pair of two points, compute their forces against each other,
 * their 'bubbles'.
 */
function computeDirectionVectors(points, params)
{
    for(let i=0; i<points.length; i++)
    {
        for(let j=i+1; j < points.length; j++)
        {
            const vector = computeDirectionVector(points[i], points[j], params);
            points[i].vectorX += vector.x;
            points[i].vectorY += vector.y;
            points[j].vectorX -= vector.x;
            points[j].vectorY -= vector.y;
        }
    }
}


/**
 * Compute force vector of two points.
 * @return vector with 'x' and 'y' values containg force vector values.
 */
function computeDirectionVector(point, point2, params)
{
    let minForceDistance = params.minForceDistance;
    let distanceCoefficient = 0;

    const vector = {};
    vector.x = point.x - point2.x;
    vector.y = point.y - point2.y;

    // Normalize the vector
    let distance = Math.sqrt(vector.x * vector.x + vector.y * vector.y);
    if(distance !== 0 && distance <= minForceDistance)
    {
        vector.x /= distance;
        vector.y /= distance;

        // The closer two points are, the bigger is the vector.
        // Can't be negative, because of the if condition above.
        distanceCoefficient = minForceDistance - distance;
    }

    vector.x *= distanceCoefficient;
    vector.y *= distanceCoefficient;

    return vector;
}


/**
 * After force vector using all neighbours is computed, normalize its length
 * (We are interested in direction only, not length).
 */
function normalizeDirectionVectors(points)
{
    for(let i=0; i<points.length; i++)
    {
        let x = points[i].vectorX;
        let y = points[i].vectorY;
        const length = Math.sqrt(x * x + y * y);
        if(length !== 0)
        {
            points[i].vectorX /= length;
            points[i].vectorY /= length;
        }
    }
}


/**
 * Transform computed direction vectors to real position on screen.
 */
function directionVectorsToPosition(points, rectangles, params)
{
    let lineLength = params.lineLength;

    for(let i=0; i<points.length; i++)
    {
        // Too far from others
        if(points[i].vectorX === 0 && points[i].vectorY === 0)
        {
            setDefaultPosition(points[i], rectangles[i], lineLength);
            continue;
        }

        // The following code ensures that the line will always have
        // the specified length no matter the angle. Without this, wide labels
        // have short line when on the right/left and long lines when on
        // the top/bottom.

        // Compute angle delimiter - if the line-angle is lower, use vertical
        // cathetus. If higher, use horizontal cathetus.
        let halfWidth = rectangles[i].width / 2;
        let halfHeight = rectangles[i].height / 2;
        let delimiterTan = halfHeight / halfWidth;
        let delimiterAngle = Math.atan(delimiterTan); // In radians.

        //
        // Compute the length of the line under the rectangle.
        //

        // noinspection JSSuspiciousNameCombination
        let cathetus = Math.abs(points[i].vectorY);
        let hypotenuse = 1; // The direction vector is normalized.
        let lineAngle = Math.asin( cathetus / hypotenuse );

        let lineUnderRect;
        if(lineAngle < delimiterAngle)
            lineUnderRect = halfWidth / Math.cos(lineAngle);
        else
            lineUnderRect = halfHeight / Math.cos(Math.PI/2 - lineAngle);

        // Compute the real end of the line (beginning in the point).
        let lineEndX = points[i].vectorX * (lineLength + lineUnderRect);
        let lineEndY = points[i].vectorY * (lineLength + lineUnderRect);

        rectangles[i].x = points[i].x + lineEndX - halfWidth;
        rectangles[i].y = points[i].y + lineEndY - halfHeight;
    }
}

/**
 * Set default label position (top right).
 */
function setDefaultPosition(point, rect, offset)
{
    let xLine = Math.cos(Math.PI/8) * offset;
    let yLine = Math.sin(Math.PI/8) * offset;
    rect.x = round(point.x + xLine);
    rect.y = round(point.y - yLine - rect.height);
}


/**
 * Compute overlapArea of each rectangle.
 */
function computeOverlaps(rectangles, points, width, height)
{
    for(let i = 0; i < rectangles.length; i++)
    {
        const overlapArea = computeScreenOverlap(rectangles[i], width, height);
        rectangles[i].overlapArea = overlapArea;
        points[i].overlapped = false;
        points[i].lineOverlapped = false;
    }

    for(let i = 0; i < rectangles.length; i++)
    {
        // In corner there can be point over the self rectangle
        if(isPointInRect(points[i], rectangles[i]))
        {
            const overlapArea = pointRectFitness(points[i], rectangles[i]);
            rectangles[i].overlapArea += overlapArea;
            points[i].overlapped = true;
        }

        for(let j = i+1; j < rectangles.length; j++)
        {
            // Add rect-to-rect overlaps
            const area = computeRectOverlapArea(rectangles[i], rectangles[j]);
            rectangles[i].overlapArea += area/2;
            rectangles[j].overlapArea += area/2;

            // Add rect[i] to point[j] overlaps
            if(isPointInRect(points[j], rectangles[i]))
            {
                // The closer is point to the rectangle center, the worse.
                const overlapArea = pointRectFitness(points[j], rectangles[i]);
                rectangles[i].overlapArea += overlapArea;
                points[j].overlapped = true;
            }

            // Add rect[j] to point[i] overlaps
            if(isPointInRect(points[i], rectangles[j]))
            {
                const overlapArea = pointRectFitness(points[i], rectangles[j]);
                rectangles[j].overlapArea += overlapArea;
                points[i].overlapped = true;
            }
        }
    }
}


/**
 * Compute a rectangle that represents an overlapping area of two rectangles.
 */
function computeOverlapRect(rectA, rectB)
{
    const rect = {};

    // Find x-intersection:
    const xL = Math.max(rectA.x, rectB.x);
    const xR = Math.min(rectA.x + rectA.width, rectB.x + rectB.width);

    // x-axis is overlapping
    if (xR > xL)
    {
        // Find y-intersection
        const yT = Math.max(rectA.y, rectB.y);
        const yB = Math.min(rectA.y + rectA.height, rectB.y + rectB.height);

        // y-axis is overlapping
        if (yB > yT)
        {
            // Calculate intersection rectangle
            rect.x = round(xL);
            rect.y = round(yT);
            rect.width = Math.abs(xR - xL);
            rect.height = Math.abs(yB - yT);

            // Fix very small numbers causing infinite loop
            rect.width = round(rect.width);
            rect.height = round(rect.height);

            return rect;
        }
    }

    // No overlap
    rect.x = 0;
    rect.y = 0;
    rect.width = 0;
    rect.height = 0;
    return rect;
}


/**
 * Get the total area of the overlapping part of two rectangles.
 */
function computeRectOverlapArea(rectA, rectB)
{
    const rect = computeOverlapRect(rectA, rectB);
    return rect.width * rect.height;
}


/**
 * Round a number down or up based on the value (0.5).
 */
function round(number)
{
    const int = Math.trunc(number);

    if((number - int) >= 0.5 )
        return Math.ceil(number);
    else
        return Math.round(number);
}


/**
 * If a part of a label rectangle is outside the screen, treat is as an overlap.
 */
function computeScreenOverlap(rect, width, height)
{
    const screenRect = {};
    screenRect.x = 0;
    screenRect.y = 0;
    screenRect.width = width;
    screenRect.height = height;

    const overlap = computeRectOverlapArea(rect, screenRect);
    const area = rect.width * rect.height;

    // If all on the screen, the returned overlap is 0
    return round(area - overlap);
}


/**
 * Check if the point is inside the rectangle.
 */
function isPointInRect(point, rect)
{
    // Point collision
    const insideAxisX = (point.x >= rect.x && point.x <= rect.x + rect.width);
    const insideAxisY = (point.y >= rect.y && point.y <= rect.y + rect.height);
    return insideAxisX && insideAxisY;
}


/**
 * Compute fitness value of the point-rectangle distance.
 */
function pointRectFitness(point, rectangle)
{
    const center = rectangleCenter(rectangle);
    const distanceX = Math.abs(point.x - center.x);
    const distanceY = Math.abs(point.y - center.y);

    // Manhattan distance
    return rectangle.width/2 + rectangle.height/2 - distanceX - distanceY;
}


/**
 * Compute the overlap score of all rectangles.
 */
function overlapScore(points, rectangles, width, height)
{
    computeOverlaps(rectangles, points, width, height);

    let overlapSum = 0;
    for(let i=0; i<rectangles.length; i++)
    {
        overlapSum += rectangles[i].overlapArea;

        if(points[i].lineOverlapped === true)
        {
            overlapSum += 10;
        }
    }

    return overlapSum;
}


/**
 * Get the center point of the rectangle.
 */
function rectangleCenter(rectangle)
{
    const point = {};
    point.x = rectangle.x + rectangle.width / 2;
    point.y = rectangle.y + rectangle.height / 2;
    return point;
}


/**
 * Vector gradient descent labeling
 * Every step only one rectangle is moved
 */
function singleGradientDescent(points, rectangles, width, height, params)
{
    const maxSteps = 1000;
    const chunkSize = 100;
    const stepChunks = maxSteps / chunkSize;

    for(let i = 0; i < stepChunks; i++)
    {
        const overlapBefore = overlapScore(points, rectangles, width, height);

        for(let j = 0; j < chunkSize; j++)
        {
            singleGradientStep(points, rectangles, width, height, params);
        }

        const overlapNow = overlapScore(points, rectangles, width, height);
        const overlapDelta = overlapBefore - overlapNow;

        // No more improvements
        if (overlapDelta <= 0) break;
    }

    // Finish computing
    computeOverlaps(rectangles, points, width, height);
}


/**
 * Labeling using gradient descent. At most one label per step.
 * Used for improving force vector algorithm to resolve little overlaps that
 * usually occur when the force vector algorithm is used.
 */
function singleGradientStep(points, rectangles, width, height, params)
{
    // A rectangle that represents a point with force-radius.
    let pointRect = {};

    for(let i=0; i<rectangles.length; i++)
    {
        // Start with zero movement
        rectangles[i].deltaX = 0;
        rectangles[i].deltaY = 0;
    }

    // Compare each pair of rectangles and compute movement vector for
    // each rectangle
    for(let i=0; i<rectangles.length; i++)
    {
        // Start with zero movement
        rectangles[i].deltaX = 0;
        rectangles[i].deltaY = 0;
        for(let j=0; j<rectangles.length; j++)
        {
            if(i===j)
            {
                // In corner case there can be point over the self rectangle
                sgd_resetPointRectangle(pointRect, points[i], params);
                sgd_addRectOverlapDelta(pointRect, rectangles[i]);

                // Add the inverse of 'point-rect' delta to the real rect.
                rectangles[i].deltaX -= pointRect.deltaX;
                rectangles[i].deltaY -= pointRect.deltaY;
                continue;
            }

            // Rect-rect
            sgd_addRectOverlapDelta(rectangles[i], rectangles[j]);

            // Point-rect
            sgd_resetPointRectangle(pointRect, points[i], params);
            sgd_addRectOverlapDelta(pointRect, rectangles[j]);

            // Add the inverse of 'point-rect' delta to the real rect.
            rectangles[j].deltaX -= pointRect.deltaX;
            rectangles[j].deltaY -= pointRect.deltaY;

            // Rect-point
            sgd_resetPointRectangle(pointRect, points[j], params);
            sgd_addRectOverlapDelta(pointRect, rectangles[i]);

            // Add the inverse of 'point-rect' delta to the real rect.
            rectangles[i].deltaX -= pointRect.deltaX;
            rectangles[i].deltaY -= pointRect.deltaY;
        }
        sgd_addScreenOverlapDelta(rectangles[i], width, height);

        // Apply deltas
        rectangles[i].x += rectangles[i].deltaX / 2;
        rectangles[i].y += rectangles[i].deltaY / 2;
    }
}


/**
 * Reset the rectangle that represents a point.
 */
function sgd_resetPointRectangle(pointRectangle, point, params)
{
    let pointRadius = params.pointRadius;

    pointRectangle.x = point.x - pointRadius;
    pointRectangle.y = point.y - pointRadius;
    pointRectangle.width = 2 * pointRadius;
    pointRectangle.height = 2 * pointRadius;
    pointRectangle.deltaX = 0;
    pointRectangle.deltaY = 0;
}


/**
 * Compute force vectors of two overlapping rectangles.
 */
function sgd_addRectOverlapDelta(rectA, rectB)
{
    const rect = computeOverlapRect(rectA, rectB);

    // No overlap, skip.
    if(rect.width === 0 || rect.height === 0) return;

    // Overlap found. Move both rectangles  half the distance to
    // eliminate this overlap, but prefer more the shorter one.
    let deltaX = 0;
    let deltaY = 0;
    if(rect.width > rect.height)
    {
        deltaX = Math.sign(rect.width);
        deltaY = rect.height;
    }
    else
    {
        deltaX = rect.width;
        deltaY = Math.sign(rect.height);
    }

    // Each rectangle will move a half of the delta, so we have to
    // determine direction of the move based od rectangle centers.
    let centerI = rectangleCenter(rectA);
    let centerJ = rectangleCenter(rectB);
    let centerOverlap = rectangleCenter(rect);

    // x-direction and y-direction.
    let dirXI = Math.sign( centerI.x - centerOverlap.x );
    let dirYI = Math.sign( centerI.y - centerOverlap.y );
    let dirXJ = Math.sign( centerJ.x - centerOverlap.x );
    let dirYJ = Math.sign( centerJ.y - centerOverlap.y );

    // If both rectangles have the same x or y position, direction is 0,
    // so we have to decide which one rectangle will move which
    // direction - the lower index has priority to move up / left.
    if(dirXI === dirXJ === 0)
    {
        dirXI = -1;
        dirXJ = 1;
    }
    if(dirYI === 0 && dirYJ === 0)
    {
        dirYI = -1;
        dirYJ = 1;
    }

    // Add to movement vectors.
    rectA.deltaX += dirXI * deltaX;
    rectA.deltaY += dirYI * deltaY;
    rectB.deltaX += dirXJ * deltaX;
    rectB.deltaY += dirYJ * deltaY;
}


/**
 * Compute overlap of the outside-screen.
 */
function sgd_addScreenOverlapDelta(rect, width, height)
{
    // Top
    rect.deltaY += Math.max(-rect.y, 0);

    // Right
    rect.deltaX -= Math.max(rect.x + rect.width - width, 0);

    // Bottom
    rect.deltaY -= Math.max(rect.y + rect.height - height, 0);

    // Left
    rect.deltaX += Math.max(-rect.x, 0);
}

function SGD_one_step(points, rectangles, width, height)
{
    const overlapBefore = overlapScore(points, rectangles, width, height);

    singleGradientStep(points, rectangles, width, height, defaultParams);

    const overlapNow = overlapScore(points, rectangles, width, height);
    const fitnessDelta = overlapBefore - overlapNow;
    console.log("fitness: " + overlapNow);
    console.log("fitness delta: " + fitnessDelta);
}
