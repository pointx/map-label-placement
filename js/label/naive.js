
function randomRange(from, to) {
    return Math.floor( Math.random() * ( 1 + from - to) ) + to;
}


/**
 * Naive labeling
 * Each rect is placed to the top-right of the point (with some offset)
 */
function topRight(points, rectangles, width, height)
{
    var solution = zeroArray(rectangles.length);
    setPositions(points, rectangles, solution);
    computeOverlaps(rectangles, points, width, height);
}


function random(points, rectangles, width, height)
{
    var solution = zeroArray(rectangles.length);
    for(var i=0; i<rectangles.length; i++)
    {
        solution[i] = Math.floor(randomRange(0, 7));
    }

    setPositions(points, rectangles, solution);
    computeOverlaps(rectangles, points, width, height);
}



