
/**
 * Labeling using simulated annealing algorithm
 */
function simulatedAnnealing(points, rectangles, width, height)
{
    // Start from 'optimal' solution
    var solution = zeroArray(rectangles.length);

    // Compute gradient - best position change that improves total overlap
    // Note: for N nodes there are (position count - 1) * N different operations
    while(true)
    {
        var operation = computeBestOperation(solution, points, rectangles, width, height);

        // Finish when there is no improving operation
        if(operation.improvement >= 0) break;

        solution[operation.index] = operation.newPosition;
    }

    // Finish computing
    setPositions(points, rectangles, solution);
    computeOverlaps(rectangles, points, width, height);
}


function computeBestOperation(solution, points, rectangles, width, height)
{
    var actualFitness = discreteFitness(solution, points, rectangles, width, height);
    var operation = {};

    operation.improvement = Number.POSITIVE_INFINITY;
    operation.index = -1;
    operation.newPosition = -1;

    // For each point calculate best position change
    for(var i=0; i<points.length; i++)
    {
        // Try every other position except the existing one
        for(var j=0; j<8; j++)
        {
            // Exclude existing position
            if(solution[i] === j) continue;

            // Exclude 'green' rectangles
            if(rectangles[i].overlapArea === 0 && points[i].lineOverlapped === false) continue;

            var previousPosition = solution[i];
            solution[i] = j;
            var fitness = discreteFitness(solution, points, rectangles, width, height);
            var improvement = fitness - actualFitness;

            // Better solution found
            if(improvement < operation.improvement)
            {
                operation.improvement = improvement;
                operation.index = i;
                operation.newPosition = j;
            }

            // Undo solution modification
            solution[i] = previousPosition;
        }
    }

    return operation;
}