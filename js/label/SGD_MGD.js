
/**
 * Combination of discrete gradient descent and singlr gradient descent

 */
function SGD_MGD(points, rectangles, width, height)
{
    // Start from all-zero solution
    var solution = zeroArray(rectangles.length);
    setPositions(points, rectangles, solution);

    // DGD
    while(true)
    {
        var operation = computeBestOperation(solution, points, rectangles, width, height);

        // Finish when there is no improving operation
        if(operation.improvement >= 0) break;

        solution[operation.index] = operation.newPosition;
    }

    setPositions(points, rectangles, solution);
    singleGradientDescent(points, rectangles, width, height, false);
}
