
/**
 * Combination of discrete gradient descent and multi gradient descent

 */
function DGD_MGD(points, rectangles, width, height)
{
    // Start from all-zero solution
    var solution = zeroArray(rectangles.length);
    setPositions(points, rectangles, solution);

    // DGD
    while(true)
    {
        var operation = computeBestOperation(solution, points, rectangles, width, height);

        // Finish when there is no improving operation
        if(operation.improvement >= 0) break;

        solution[operation.index] = operation.newPosition;
    }

    setPositions(points, rectangles, solution);

    // MGD
    while(true)
    {
        var fitnessDelta = multiGradientStep(points, rectangles, width, height);
        if(fitnessDelta >= 0) break;
    }

    // Finish computing
    computeOverlaps(rectangles, points, width, height);
}
