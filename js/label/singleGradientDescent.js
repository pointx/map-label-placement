

function SGD_one_step(points, rectangles, width, height)
{
    var fitnessDelta = singleGradientStep(points, rectangles, width, height);
    console.log("fitness delta: " + fitnessDelta);
}

/**
 * Vector gradient descent labeling
 * Every step only one rectangle is moved
 */
function singleGradientDescent(points, rectangles, width, height, reset = true)
{
    if(reset)
    {
        // Start from all-zero solution
        var solution = zeroArray(rectangles.length);
        setPositions(points, rectangles, solution);
    }

    // Set limit to while loop
    var loops = 0;
    var LOOPS_MAX = rectangles.length * glob_loop_coefficient;

    // Compute gradient - best position change that improves total overlap
    // Note: since we use continuous space, we can not use predefined positions
    // anymore.
    var lastFitnessDelta = [-1, -2];
    while(true)
    {
        // Buggy behaviour - to much steps (infitite loop probably)
        if(loops >= LOOPS_MAX)
        {
            console.log("max loops reached: " + loops);
            break;
        }

        var fitnessDelta = singleGradientStep(points, rectangles, width, height);
        console.log("fitness delta: " + fitnessDelta);

        // Zero-overlap solution found
        if(fitnessDelta === 0 && lastFitnessDelta[0] === 0 && lastFitnessDelta[1] === 0) break;

        lastFitnessDelta[0] = fitnessDelta;
        lastFitnessDelta[1] = lastFitnessDelta[0];
        loops++;
    }

    // Finish computing
    computeOverlaps(rectangles, points, width, height);
}


function singleGradientStep(points, rectangles, width, height)
{
    var actualFitness = continuousFitness(points, rectangles, width, height);

    for(var i=0; i<rectangles.length; i++)
    {
        // Start with zero movement
        rectangles[i].deltaX = 0;
        rectangles[i].deltaY = 0;
    }

    // Compare each pair of rectangles and compute movement vector for
    // each rectangle
    for(i=0; i<rectangles.length; i++)
    {
        // Start with zero movement
        rectangles[i].deltaX = 0;
        rectangles[i].deltaY = 0;

        for(var j=0; j<rectangles.length; j++)
        {
            if(i===j)
            {
                // In corner there can be point over the self rectangle
                addPointOverlapDelta(points[i], rectangles[i]);
                continue;
            }

            sgd_addRectOverlapDelta(rectangles[i], rectangles[j]);
            addPointOverlapDelta(points[i], rectangles[j]);
            addPointOverlapDelta(points[j], rectangles[i]);
        }
        sgd_addScreenOverlapDelta(rectangles[i], width, height);

        // Apply deltas
        rectangles[i].x += rectangles[i].deltaX / 2;
        rectangles[i].y += rectangles[i].deltaY / 2;
    }

    var fitness = continuousFitness(points, rectangles, width, height);
    var improvement = fitness - actualFitness;

    /*if(improvement >= 0)
    {
        // Undo deltas
        for(i=0; i<rectangles.length; i++)
        {
            rectangles[i].x -= rectangles[i].deltaX;
            rectangles[i].y -= rectangles[i].deltaY;
        }
    }*/

    return improvement;
}