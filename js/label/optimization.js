//
// Generic map labeling engine
//
// Input:
//
// - points (n-sized list of objects with x and y numbers)
// - rectangles (n-sized list of objects with x, y, width, height)
// - canvas dimension: width, height
//
// Output:
//
// - n-sized list of points describing top-left corner of each rectangle
// - each rectangle has new attribute: overlapArea
//

//
// Structures and methods for using optimization methods to solve map label
// placement problem.
//
function zeroArray(length)
{
    // https://stackoverflow.com/a/13735425
    var array = new Array(length);
    return Array.apply(null, array).map(Number.prototype.valueOf,0);
}


/**
 * Set a position to the rectangle
 */
function setPosition(point, rect, positionIndex)
{
    var offset = 50;
    switch(positionIndex)
    {
        // top right
        case 0:
            rect.x = point.x + Math.cos(Math.PI/8) * offset;
            rect.y = point.y - Math.sin(Math.PI/8) * offset - rect.height;
            break;

        // top left
        case 1:
            rect.x = point.x - Math.cos(Math.PI/8) * offset - rect.width;
            rect.y = point.y - Math.sin(Math.PI/8) * offset - rect.height;
            break;

        // bottom left
        case 2:
            rect.x = point.x - Math.cos(Math.PI/8) * offset - rect.width;
            rect.y = point.y + Math.sin(Math.PI/8) * offset;
            break;

        // bottom right
        case 3:
            rect.x = point.x + Math.cos(Math.PI/8) * offset;
            rect.y = point.y + Math.sin(Math.PI/8) * offset;
            break;

        // right
        case 4:
            rect.x = point.x + offset;
            rect.y = point.y - rect.height/2;
            break;

        // top
        case 5:
            rect.x = point.x - rect.width/2;
            rect.y = point.y - offset - rect.height;
            break;

        // left
        case 6:
            rect.x = point.x - offset - rect.width;
            rect.y = point.y - rect.height/2;
            break;

        // bottom
        case 7:
            rect.x = point.x - rect.width/2;
            rect.y = point.y + offset;
            break;
    }

    rect.x = round(rect.x);
    rect.y = round(rect.y);
}



function computeOverlaps(inputRectArray, outputRectArray)
{
    // Empty overlaps
    while(outputRectArray.length > 0) {
        outputRectArray.pop();
    }

    for(var i=0; i<inputRectArray.length; i++)
    {
        // Each pair only once
        for (var j = i + 1; j < inputRectArray.length; j++)
        {
            var overlap = computeOverlapRect(inputRectArray[i], inputRectArray[j]);
            var area = overlap.width * overlap.height;

            if(area > 0)
            {
                outputRectArray.push(overlap);
            }
        }
    }
}



/**
 * Set rect positions according to position index in solution array
 */
function setPositions(points, rectangles, solution)
{
    for(var i=0; i<rectangles.length; i++)
    {
        var positionIndex = solution[i];
        setPosition(points[i], rectangles[i], positionIndex);
    }
}

function computeOverlapRect(rectA, rectB)
{
    var rect = {};

    // Find x-intersection:
    var xL = Math.max(rectA.x, rectB.x);
    var xR = Math.min(rectA.x + rectA.width, rectB.x + rectB.width);

    // x-axis is overlapping
    if (xR > xL)
    {
        // Find y-intersection
        var yT = Math.max(rectA.y, rectB.y);
        var yB = Math.min(rectA.y + rectA.height, rectB.y + rectB.height);

        // y-axis is overlapping
        if (yB > yT)
        {
            // Calculate intersection rectangle
            rect.x = round(xL);
            rect.y = round(yT);
            rect.width = Math.abs(xR - xL);
            rect.height = Math.abs(yB - yT);

            // Fix very small numbers causing infinite loop
            rect.width = round(rect.width);
            rect.height = round(rect.height);

            return rect;
        }
    }

    // No overlap
    rect.x = 0;
    rect.y = 0;
    rect.width = 0;
    rect.height = 0;
    return rect;
}


function computeRectOverlapArea(rectA, rectB)
{
    var rect = computeOverlapRect(rectA, rectB);
    return rect.width * rect.height;
}


function round(number)
{
    var int = Math.trunc(number);

    if((number - int) >= 0.5 )
    {
        return Math.ceil(number);
    }
    else
    {
        return Math.round(number);
    }
}

function computeScreenOverlap(rect, width, height)
{
    var screenRect = {};
    screenRect.x = 0;
    screenRect.y = 0;
    screenRect.width = width;
    screenRect.height = height;

    var overlap = computeRectOverlapArea(rect, screenRect);
    var area = rect.width * rect.height;
    
    // If all on the screen, the returned overlap is 0
    return round(area - overlap);
}


function isPointInRect(point, rect)
{
    // Point collision
    var inHorizontally = (point.x >= rect.x && point.x <= rect.x + rect.width);
    var inVertically = (point.y >= rect.y && point.y <= rect.y + rect.height);
    return inHorizontally && inVertically;

    // Represent a point as a rect
    //var pointRect = {};
    //pointRect.x = point.x - glob_point_offset/2;
    //pointRect.y = point.y - glob_point_offset/2;
    //pointRect.width = glob_point_offset;
    //pointRect.height = glob_point_offset;
    //return computeRectOverlapArea(pointRect, rect) > 0;
}


function isLineInLine(pointA, rectangleA, pointB, rectangleB)
{
    // Compute lines
    var a1 = pointA;
    var a2 = rectangleCenter(rectangleA);
    var b1 = pointB;
    var b2 = rectangleCenter(rectangleB);

    return linesInteresction(a1, a2, b1, b2);
}



function pointRectFitness(point, rectangle)
{
    var center = rectangleCenter(rectangle);
    var distanceX = Math.abs(point.x - center.x);
    var distanceY = Math.abs(point.y - center.y);

    // Manhattan distance
    return rectangle.width/2 + rectangle.height/2 - distanceX - distanceY;
}

/**
 * Set overlapArea to each rectangle
 */
function setOverlaps(rectangles, points, width, height)
{
    for(var i=0; i<rectangles.length; i++)
    {
        rectangles[i].overlapArea = computeScreenOverlap(rectangles[i], width, height);
        points[i].overlapped = false;
        points[i].lineOverlapped = false;
    }

    for(i=0; i<rectangles.length; i++)
    {
        // In corner there can be point over the self rectangle
        if(isPointInRect(points[i], rectangles[i]))
        {
            rectangles[i].overlapArea += pointRectFitness(points[i], rectangles[i]);
            points[i].overlapped = true;
        }

        for(var j=i+1; j<rectangles.length; j++)
        {
            // Add rect-to-rect overlaps
            var area = computeRectOverlapArea(rectangles[i], rectangles[j]);
            rectangles[i].overlapArea += area/2;
            rectangles[j].overlapArea += area/2;

            // Add rect[i] to point[j] overlaps
            if(isPointInRect(points[j], rectangles[i]))
            {
                // The closer is point to the rectangle center, the worse.
                rectangles[i].overlapArea += pointRectFitness(points[j], rectangles[i]);
                points[j].overlapped = true;
            }

            // Add rect[j] to point[i] overlaps
            if(isPointInRect(points[i], rectangles[j]))
            {
                rectangles[j].overlapArea += pointRectFitness(points[i], rectangles[j]);
                points[i].overlapped = true;
            }

            // Add lines overlap
            if(isLineInLine(points[i], rectangles[i], points[j], rectangles[j]))
            {
                points[i].lineOverlapped = true;
                points[j].lineOverlapped = true;
                rectangles[i].overlapArea += 25;
                rectangles[j].overlapArea += 25;
                //console.log(i + ", " + j + " interescting.");
            }
            else
            {
                //console.log(i + ", " + j + " not interescting.");
            }
        }
    }
}


function discreteFitness(solution, points, rectangles, width, height)
{
    setPositions(points, rectangles, solution);
    setOverlaps(rectangles, points, width, height);
    
    var positionSum = 0;
    var overlapSum = 0;
    for(var i=0; i<solution.length; i++)
    {
        positionSum += solution[i];
        overlapSum += rectangles[i].overlapArea;

        if(points[i].lineOverlapped === true)
        {
            overlapSum += 10;
        }
    }

    return positionSum + overlapSum;
}


function continuousFitness(points, rectangles, width, height)
{
    setOverlaps(rectangles, points, width, height);

    var overlapSum = 0;
    for(var i=0; i<rectangles.length; i++)
    {
        overlapSum += rectangles[i].overlapArea;

        if(points[i].lineOverlapped === true)
        {
            overlapSum += 10;
        }
    }

    return overlapSum;
}


function rectangleCenter(rectangle)
{
    var point = {};
    point.x = rectangle.x + rectangle.width / 2;
    point.y = rectangle.y + rectangle.height / 2;
    return point;
}

/**
 * Returs true if the lines (a1 -> a2), (b1 -> b2) intersect
 */
function linesInteresction(a1, a2, b1, b2)
{
    var x1 = a1.x;
    var x2 = a2.x;
    var x3 = b1.x;
    var x4 = b2.x;
    var y1 = a1.y;
    var y2 = a2.y;
    var y3 = b1.y;
    var y4 = b2.y;

    // https://stackoverflow.com/a/37265703
    var a_dx = x2 - x1;
    var a_dy = y2 - y1;
    var b_dx = x4 - x3;
    var b_dy = y4 - y3;
    var s = (-a_dy * (x1 - x3) + a_dx * (y1 - y3)) / (-b_dx * a_dy + a_dx * b_dy);
    var t = (+b_dx * (y1 - y3) - b_dy * (x1 - x3)) / (-b_dx * a_dy + a_dx * b_dy);
    return (s >= 0 && s <= 1 && t >= 0 && t <= 1);
}